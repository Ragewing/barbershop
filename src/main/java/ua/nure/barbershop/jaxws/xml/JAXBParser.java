package ua.nure.barbershop.jaxws.xml;

import org.xml.sax.SAXException;
import ua.nure.barbershop.jaxws.Const;
import ua.nure.barbershop.jaxws.xml.entities.BarbershopType;

import javax.xml.XMLConstants;
import javax.xml.bind.*;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;

public class JAXBParser {
    private static JAXBParser instance;
    public BarbershopType orders;

    public void save() {
        if (orders != null) {
            try {
                saveOrders(orders, Const.XML_FILE, Const.XSD_FILE, Const.OBJECT_FACTORY);
            } catch (JAXBException | SAXException e) {
                e.printStackTrace();
            }
        }
    }

    public BarbershopType loadOrders(final String xmlFileName,
                                     final String xsdFileName, Class<?> objectFactory) throws JAXBException, SAXException {
        JAXBContext jc = JAXBContext.newInstance(objectFactory);
        Unmarshaller unmarshaller = jc.createUnmarshaller();

        // obtain schema
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        if (xsdFileName != null) { // <-- setup validation on
            Schema schema = null;
            if ("".equals(xsdFileName)) {
                // setup validation against XSD pointed in XML
                schema = sf.newSchema();
            } else {
                // setup validation against external XSD
                schema = sf.newSchema(new File(xsdFileName));
            }

            unmarshaller.setSchema(schema); // <-- set XML schema for validation

            // set up handler
            unmarshaller.setEventHandler(new ValidationEventHandler() {
                // this method will be invoked if XML is NOT valid
                @Override
                public boolean handleEvent(ValidationEvent event) {
                    // at this point we have NOT valid XML document
                    // just print message
                    System.err.println("====================================");
                    System.err.println(xmlFileName + " is NOT valid against "
                            + xsdFileName + ":\n" + event.getMessage());
                    System.err.println("====================================");

                    // if we place 'return false;' unmarshal method throws
                    // exception
                    // 'return true;' does not imply any exceptions
                    return true;
                }
            });
        }

        // do unmarshal
        Source source = new StreamSource(new File(xmlFileName));
        JAXBElement<BarbershopType> orders = unmarshaller.unmarshal(source, BarbershopType.class);
        return orders.getValue();
    }

    public static void saveOrders(BarbershopType orders, final String xmlFileName,
                                  final String xsdFileName, Class<?> objectFactory) throws JAXBException, SAXException {
        JAXBContext jc = JAXBContext.newInstance(objectFactory);
        Marshaller marshaller = jc.createMarshaller();

        // obtain schema
        SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        // setup validation against XSD
        if (xsdFileName != null) {
            Schema schema = sf.newSchema(new File(xsdFileName));

            marshaller.setSchema(schema);
            marshaller.setEventHandler(event -> {
                // at this point we have NOT valid XML document
                // just print message
                System.err.println("====================================");
                System.err.println(xmlFileName + " is NOT valid against "
                        + xsdFileName + ":\n" + event.getMessage());
                System.err.println("====================================");

                // if we place 'return false;' marshal method throws
                // exception
                // 'return true;' does not imply any exceptions
                return true;
            });
        }

        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, Const.SCHEMA_LOCATION_URI);
        marshaller.marshal(orders, new File(xmlFileName));
    }

    private JAXBParser() {
        try {
            orders = loadOrders(Const.XML_FILE, Const.XSD_FILE, Const.OBJECT_FACTORY);
        } catch (JAXBException | SAXException e) {
            e.printStackTrace();
        }
    }

    public static JAXBParser getInstance() {
        if (instance == null) {
            instance = new JAXBParser();
        }
        return instance;
    }
}
