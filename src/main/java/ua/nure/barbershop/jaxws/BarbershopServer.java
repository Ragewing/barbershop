package ua.nure.barbershop.jaxws;

import ua.nure.barbershop.jaxws.service.BarbershopServiceImpl;

import javax.xml.ws.Endpoint;

public class BarbershopServer {
    final static String barbershopUrl = "http://localhost:4221/barbershop";
    final static Object barbershopServiceImpl = new BarbershopServiceImpl();

    public static void main(String[] args) {
        Endpoint.publish(barbershopUrl, barbershopServiceImpl);
    }
}
