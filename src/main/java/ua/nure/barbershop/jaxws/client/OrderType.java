
package ua.nure.barbershop.jaxws.client;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="customer" type="{http://nure.ua/barbershop}CustomerType"/>
 *         &lt;element name="master" type="{http://nure.ua/barbershop}MasterType"/>
 *         &lt;element name="service" type="{http://nure.ua/barbershop}ServiceType" maxOccurs="unbounded"/>
 *         &lt;element name="comment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderType", namespace = "http://nure.ua/barbershop", propOrder = {
    "customer",
    "master",
    "service",
    "comment"
})
public class OrderType {

    @XmlElement(required = true)
    protected CustomerType customer;
    @XmlElement(required = true)
    protected MasterType master;
    @XmlElement(required = true)
    protected List<ServiceType> service;
    protected String comment;

    /**
     * Gets the value of the customer property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerType }
     *     
     */
    public CustomerType getCustomer() {
        return customer;
    }

    /**
     * Sets the value of the customer property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerType }
     *     
     */
    public void setCustomer(CustomerType value) {
        this.customer = value;
    }

    /**
     * Gets the value of the master property.
     * 
     * @return
     *     possible object is
     *     {@link MasterType }
     *     
     */
    public MasterType getMaster() {
        return master;
    }

    /**
     * Sets the value of the master property.
     * 
     * @param value
     *     allowed object is
     *     {@link MasterType }
     *     
     */
    public void setMaster(MasterType value) {
        this.master = value;
    }

    /**
     * Gets the value of the service property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the service property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceType }
     * 
     * 
     */
    public List<ServiceType> getService() {
        if (service == null) {
            service = new ArrayList<ServiceType>();
        }
        return this.service;
    }

    /**
     * Gets the value of the comment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets the value of the comment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComment(String value) {
        this.comment = value;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("\nOrder[\ncustomer name: ");
        builder.append(customer.getName());
        builder.append(", surname: ");
        builder.append(customer.surname);
        if (customer.connect.email != null) {
            builder.append(", email: ");
            builder.append(customer.connect.email);
        }
        if (customer.connect.phone != null) {
            builder.append(", phone: ");
            builder.append(customer.connect.phone);
        }

        builder.append(master);
        builder.append("\n");

        builder.append("services=[");
        for (int i = 0; i < service.size(); ++i) {
            builder.append(service.get(i));
            if (i != service.size() - 1) {
                builder.append("; ");
            }
        }

        builder.append("]");
        if (comment != null) {
            builder.append(", comment: ");
            builder.append(comment);
        }
        builder.append("]");

        return builder.toString();
    }
}
