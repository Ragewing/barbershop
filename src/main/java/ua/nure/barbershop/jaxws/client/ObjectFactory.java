
package ua.nure.barbershop.jaxws.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ua.nure.barbershop.jaxws.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetMasters_QNAME = new QName("http://service.jaxws.barbershop.nure.ua/", "getMasters");
    private final static QName _AddOrderResponse_QNAME = new QName("http://service.jaxws.barbershop.nure.ua/", "addOrderResponse");
    private final static QName _GetMastersResponse_QNAME = new QName("http://service.jaxws.barbershop.nure.ua/", "getMastersResponse");
    private final static QName _GetOrders_QNAME = new QName("http://service.jaxws.barbershop.nure.ua/", "getOrders");
    private final static QName _GetServicesResponse_QNAME = new QName("http://service.jaxws.barbershop.nure.ua/", "getServicesResponse");
    private final static QName _Barbershop_QNAME = new QName("http://nure.ua/barbershop", "Barbershop");
    private final static QName _GetServices_QNAME = new QName("http://service.jaxws.barbershop.nure.ua/", "getServices");
    private final static QName _AddOrder_QNAME = new QName("http://service.jaxws.barbershop.nure.ua/", "addOrder");
    private final static QName _GetTotalProfitResponse_QNAME = new QName("http://service.jaxws.barbershop.nure.ua/", "getTotalProfitResponse");
    private final static QName _GetTotalProfit_QNAME = new QName("http://service.jaxws.barbershop.nure.ua/", "getTotalProfit");
    private final static QName _GetOrdersResponse_QNAME = new QName("http://service.jaxws.barbershop.nure.ua/", "getOrdersResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ua.nure.barbershop.jaxws.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CustomerType }
     * 
     */
    public CustomerType createCustomerType() {
        return new CustomerType();
    }

    /**
     * Create an instance of {@link GetTotalProfit }
     * 
     */
    public GetTotalProfit createGetTotalProfit() {
        return new GetTotalProfit();
    }

    /**
     * Create an instance of {@link GetOrdersResponse }
     * 
     */
    public GetOrdersResponse createGetOrdersResponse() {
        return new GetOrdersResponse();
    }

    /**
     * Create an instance of {@link GetTotalProfitResponse }
     * 
     */
    public GetTotalProfitResponse createGetTotalProfitResponse() {
        return new GetTotalProfitResponse();
    }

    /**
     * Create an instance of {@link AddOrder }
     * 
     */
    public AddOrder createAddOrder() {
        return new AddOrder();
    }

    /**
     * Create an instance of {@link GetServicesResponse }
     * 
     */
    public GetServicesResponse createGetServicesResponse() {
        return new GetServicesResponse();
    }

    /**
     * Create an instance of {@link GetServices }
     * 
     */
    public GetServices createGetServices() {
        return new GetServices();
    }

    /**
     * Create an instance of {@link AddOrderResponse }
     * 
     */
    public AddOrderResponse createAddOrderResponse() {
        return new AddOrderResponse();
    }

    /**
     * Create an instance of {@link GetMastersResponse }
     * 
     */
    public GetMastersResponse createGetMastersResponse() {
        return new GetMastersResponse();
    }

    /**
     * Create an instance of {@link GetOrders }
     * 
     */
    public GetOrders createGetOrders() {
        return new GetOrders();
    }

    /**
     * Create an instance of {@link GetMasters }
     * 
     */
    public GetMasters createGetMasters() {
        return new GetMasters();
    }

    /**
     * Create an instance of {@link BarbershopType }
     * 
     */
    public BarbershopType createBarbershopType() {
        return new BarbershopType();
    }

    /**
     * Create an instance of {@link OrderType }
     * 
     */
    public OrderType createOrderType() {
        return new OrderType();
    }

    /**
     * Create an instance of {@link ServiceType }
     * 
     */
    public ServiceType createServiceType() {
        return new ServiceType();
    }

    /**
     * Create an instance of {@link MasterType }
     * 
     */
    public MasterType createMasterType() {
        return new MasterType();
    }

    /**
     * Create an instance of {@link CustomerType.Connect }
     * 
     */
    public CustomerType.Connect createCustomerTypeConnect() {
        return new CustomerType.Connect();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMasters }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.jaxws.barbershop.nure.ua/", name = "getMasters")
    public JAXBElement<GetMasters> createGetMasters(GetMasters value) {
        return new JAXBElement<GetMasters>(_GetMasters_QNAME, GetMasters.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddOrderResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.jaxws.barbershop.nure.ua/", name = "addOrderResponse")
    public JAXBElement<AddOrderResponse> createAddOrderResponse(AddOrderResponse value) {
        return new JAXBElement<AddOrderResponse>(_AddOrderResponse_QNAME, AddOrderResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMastersResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.jaxws.barbershop.nure.ua/", name = "getMastersResponse")
    public JAXBElement<GetMastersResponse> createGetMastersResponse(GetMastersResponse value) {
        return new JAXBElement<GetMastersResponse>(_GetMastersResponse_QNAME, GetMastersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOrders }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.jaxws.barbershop.nure.ua/", name = "getOrders")
    public JAXBElement<GetOrders> createGetOrders(GetOrders value) {
        return new JAXBElement<GetOrders>(_GetOrders_QNAME, GetOrders.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetServicesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.jaxws.barbershop.nure.ua/", name = "getServicesResponse")
    public JAXBElement<GetServicesResponse> createGetServicesResponse(GetServicesResponse value) {
        return new JAXBElement<GetServicesResponse>(_GetServicesResponse_QNAME, GetServicesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BarbershopType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://nure.ua/barbershop", name = "Barbershop")
    public JAXBElement<BarbershopType> createBarbershop(BarbershopType value) {
        return new JAXBElement<BarbershopType>(_Barbershop_QNAME, BarbershopType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetServices }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.jaxws.barbershop.nure.ua/", name = "getServices")
    public JAXBElement<GetServices> createGetServices(GetServices value) {
        return new JAXBElement<GetServices>(_GetServices_QNAME, GetServices.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddOrder }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.jaxws.barbershop.nure.ua/", name = "addOrder")
    public JAXBElement<AddOrder> createAddOrder(AddOrder value) {
        return new JAXBElement<AddOrder>(_AddOrder_QNAME, AddOrder.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTotalProfitResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.jaxws.barbershop.nure.ua/", name = "getTotalProfitResponse")
    public JAXBElement<GetTotalProfitResponse> createGetTotalProfitResponse(GetTotalProfitResponse value) {
        return new JAXBElement<GetTotalProfitResponse>(_GetTotalProfitResponse_QNAME, GetTotalProfitResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTotalProfit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.jaxws.barbershop.nure.ua/", name = "getTotalProfit")
    public JAXBElement<GetTotalProfit> createGetTotalProfit(GetTotalProfit value) {
        return new JAXBElement<GetTotalProfit>(_GetTotalProfit_QNAME, GetTotalProfit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOrdersResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.jaxws.barbershop.nure.ua/", name = "getOrdersResponse")
    public JAXBElement<GetOrdersResponse> createGetOrdersResponse(GetOrdersResponse value) {
        return new JAXBElement<GetOrdersResponse>(_GetOrdersResponse_QNAME, GetOrdersResponse.class, null, value);
    }

}
