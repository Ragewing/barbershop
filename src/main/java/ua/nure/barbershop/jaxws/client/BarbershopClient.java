package ua.nure.barbershop.jaxws.client;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;

public class BarbershopClient {
    final static String url = "http://localhost:4221/barbershop?wsdl";

    public static void main(String[] args) throws MalformedURLException, InterruptedException {
        BarbershopServiceImplService service = new BarbershopServiceImplService(new URL(url));
        BarbershopServiceImpl client = service.getBarbershopServiceImplPort();

        System.out.println("Total profit response: " + client.getTotalProfit());
        System.out.println("Services response: " + client.getServices());
        System.out.println("Masters response: " + client.getMasters());

        OrderType order = new OrderType();
        CustomerType customer = new CustomerType();
        customer.setName("Алексей");
        customer.setSurname("Наумчук");
        customer.setSex("М");
        CustomerType.Connect connect = new CustomerType.Connect();
        connect.setEmail("oleksii.naumchuk@nure.ua");
        customer.setConnect(connect);
        order.setCustomer(customer);

        MasterType master = new MasterType();
        master.setName("Дмитрий");
        master.setSurname("Сычёв");
        master.setLogin("dimon");
        master.setPassword("nomid12345");
        master.setSalary(BigDecimal.valueOf(10000));
        order.setMaster(master);

        order.getService().add(client.getServices().get(1));

        System.out.println("Orders before adding new order: ");
        System.out.println("Orders response: " + client.getOrders());

        System.out.println("\n");
        System.out.println("Orders after adding new order: ");
        client.addOrder(order);
        System.out.println("Orders response: " + client.getOrders());
    }
}
