
package ua.nure.barbershop.jaxws.client;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BarbershopType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BarbershopType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="order" type="{http://nure.ua/barbershop}OrderType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BarbershopType", namespace = "http://nure.ua/barbershop", propOrder = {
    "order"
})
public class BarbershopType {

    @XmlElement(required = true)
    protected List<OrderType> order;

    /**
     * Gets the value of the order property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the order property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderType }
     * 
     * 
     */
    public List<OrderType> getOrder() {
        if (order == null) {
            order = new ArrayList<OrderType>();
        }
        return this.order;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Orders = [");
        if (order != null) {
            for (int i = 0; i < order.size(); ++i) {
                builder.append(order.get(i).toString());
                if (i != order.size() - 1) {
                    builder.append(", ");
                }
            }
            builder.append("]");
        }
        builder.append("]");

        return builder.toString();
    }

}
