package ua.nure.barbershop.jaxws.service;

import ua.nure.barbershop.jaxws.xml.entities.BarbershopType;
import ua.nure.barbershop.jaxws.xml.entities.MasterType;
import ua.nure.barbershop.jaxws.xml.entities.OrderType;
import ua.nure.barbershop.jaxws.xml.entities.ServiceType;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface BarbershopService {
    @WebMethod
    BarbershopType getOrders();

    @WebMethod
    MasterType[] getMasters();

    @WebMethod
    ServiceType[] getServices();

    @WebMethod
    void addOrder(OrderType order);

    @WebMethod
    int getTotalProfit();
}
