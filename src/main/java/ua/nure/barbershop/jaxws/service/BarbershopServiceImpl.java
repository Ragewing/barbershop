package ua.nure.barbershop.jaxws.service;

import org.xml.sax.SAXException;
import ua.nure.barbershop.jaxws.Const;
import ua.nure.barbershop.jaxws.xml.JAXBParser;
import ua.nure.barbershop.jaxws.xml.entities.BarbershopType;
import ua.nure.barbershop.jaxws.xml.entities.MasterType;
import ua.nure.barbershop.jaxws.xml.entities.OrderType;
import ua.nure.barbershop.jaxws.xml.entities.ServiceType;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;
import java.util.LinkedList;

@WebService
public class BarbershopServiceImpl implements BarbershopService {
    @WebMethod
    public BarbershopType getOrders() {
        try {
            return JAXBParser.getInstance().loadOrders(Const.XML_FILE, Const.XSD_FILE, Const.OBJECT_FACTORY);
        } catch (JAXBException | SAXException e) {
            e.printStackTrace();
        }

        return null;
    }

    @WebMethod
    public MasterType[] getMasters() {
        BarbershopType orders = JAXBParser.getInstance().orders;
        LinkedList<MasterType> mastersList = new LinkedList<>();
        for (int i = 0; i < orders.getOrder().size(); ++i) {
            MasterType master = orders.getOrder().get(i).getMaster();
            if (mastersList.contains(master))
                continue;
            mastersList.add(master);
        }

        MasterType[] masters = new MasterType[mastersList.size()];
        mastersList.toArray(masters);

        return masters;
    }

    @WebMethod
    public ServiceType[] getServices() {
        BarbershopType orders = JAXBParser.getInstance().orders;
        LinkedList<ServiceType> servicesList = new LinkedList<>();
        for (int i = 0; i < orders.getOrder().size(); ++i) {
            for (int j = 0; j < orders.getOrder().get(i).getService().size(); ++j) {
                ServiceType service = orders.getOrder().get(i).getService().get(j);
                if (servicesList.contains(service))
                    continue;
                servicesList.add(service);
            }
        }

        ServiceType[] services = new ServiceType[servicesList.size()];
        servicesList.toArray(services);

        return services;
    }

    @WebMethod
    public void addOrder(OrderType order) {
        JAXBParser.getInstance().orders.getOrder().add(order);
        JAXBParser.getInstance().save();
    }

    @WebMethod
    public int getTotalProfit() {
        BarbershopType orders = JAXBParser.getInstance().orders;
        int profit = 0;
        for (int i = 0; i < orders.getOrder().size(); ++i) {
            for (int j = 0; j < orders.getOrder().get(i).getService().size(); ++j) {
                profit += orders.getOrder().get(i).getService().get(j).getPrice();
            }
        }

        return profit;
    }
}
