package ua.nure.barbershop.jaxws;

import ua.nure.barbershop.jaxws.xml.entities.ObjectFactory;

public interface Const {

    String XML_FILE = "barbershop.xml";
    String XSD_FILE = "barbershop.xsd";
    Class<?> OBJECT_FACTORY = ObjectFactory.class;

    String SCHEMA_LOCATION_URI = "http://nure.ua/barbershop pz_1.xsd";

}
